﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineProjectileMotion : MonoBehaviour
{
    public float MoveSpeed;
    public float Amplitude;
    public float Frequency;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        var upvector = Mathf.Sin(Time.time * Frequency) * Vector3.up * Amplitude;
        var forwardVector = Vector3.right;
        transform.Translate((forwardVector + upvector) * MoveSpeed);
    }
}
