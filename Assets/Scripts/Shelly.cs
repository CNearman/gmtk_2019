﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shelly : MonoBehaviour
{
    public float speed;
    public float dead;
    public float rotspeed;

    public float playerDamageGrace;

    public GameObject bloodSplat;
    public GameObject bloodSplatCact;

    bool isDone;

    float xm;
    float ym;

    private void Awake()
    {
        EventManager.StartListening("R_XAxis_1", SetH);
        EventManager.StartListening("R_YAxis_1", SetV);
    }

    // Start is called before the first frame update
    void Start()
    {
        isDone = false;
    }

    void SetH(EventBody eb)
    {
        xm = ((AxisEB)eb).value;
    }

    void SetV(EventBody eb)
    {
        ym = -((AxisEB)eb).value;
    }


    // Update is called once per frame
    void Update()
    {
        if(!isDone)
        {
            if (playerDamageGrace >= 0)
            {
                playerDamageGrace -= Time.deltaTime;
            }

            Vector2 stickIn = new Vector2(xm, ym);

            if (stickIn.magnitude > dead)
            {
                Vector3 dir = new Vector3(xm, ym, 0f);
                Vector3 point = dir;
                //Quaternion rotty = Quaternion.LookRotation(point);

                //transform.rotation = Quaternion.Slerp(transform.rotation, rotty, rotspeed * Time.deltaTime);

                //float rot = Mathf.Atan2(xm, ym) * Mathf.Rad2Deg;// - 90;

                //if(rot < 0)
                //{
                //    rot += 365;
                //}

                //Debug.Log(xm + ", " + ym);

                Vector3 poo = Vector3.RotateTowards(transform.up, point.normalized, rotspeed * Time.deltaTime, 0f);
                Debug.DrawRay(transform.position, poo, Color.red);
                //Debug.Log(poo);

                //transform.rotation = Quaternion.Euler(0f, 0f, rot);
                //transform.rotation = Quaternion.LookRotation(poo, Vector3.forward);
                Quaternion dick = Quaternion.identity;
                dick.SetLookRotation(Vector3.forward, poo);

                transform.rotation = dick;
            }

            transform.Translate(transform.up * speed * Time.deltaTime, Space.World);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            //Destroy(gameObject);
            gameObject.GetComponent<TrailRenderer>().startColor = Color.black;
            isDone = true;
            gameObject.GetComponent<CapsuleCollider2D>().isTrigger = false;
            gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }
        else if (collision.gameObject.tag == "Player")
        {
            if(playerDamageGrace < 0)
            {
                GameObject bloodPuff = Instantiate(bloodSplat, gameObject.transform.position, Quaternion.identity);
                Destroy(bloodPuff, 5f);
                EventManager.TriggerEvent(collision.gameObject.name + "_Damage", null);
            }
        }
        else if (collision.gameObject.tag == "Enemy")
        {
            GameObject bloodPuff = Instantiate(bloodSplatCact, gameObject.transform.position, Quaternion.identity);
            Destroy(bloodPuff, 5f);
            EventManager.TriggerEvent(collision.gameObject.name + "_Damage", null);
        }
        else if(collision.gameObject.tag == "TNT")
        {
            EventManager.TriggerEvent("TriggerTnt_" + collision.gameObject.GetComponent<TntPlunger>().tntWireGroup, null);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            EventManager.TriggerEvent("PickUpShelly", null);
            Destroy(gameObject);
        }
    }

}
