﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float speed;
    public float maxSpeed;

    public float jumpForce;
    public float gravity;

    public float maxHealth;
    float health;
    public Image healthBar;
    public Image reloadHead;
    public Image reloadTop;
    public Text reloadTopText;

    public float reloadTime;
    float currentReloadTime;
    bool isReloading;
    
    public GameObject shellyPrefab;

    bool hasBullet;
    bool facingRight;

    private void Awake()
    {
        EventManager.StartListening("RB_1", Jump);
        EventManager.StartListening("LB_1", Fire);
        EventManager.StartListening("L_XAxis_1", MoveH);
        EventManager.StartListening("L_YAxis_1", MoveV);
        EventManager.StartListening(gameObject.name + "_Damage", Damage);
        EventManager.StartListening("PickUpShelly", PickUpShelly);
    }

    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        reloadHead.gameObject.SetActive(false);
        hasBullet = true;
        facingRight = true;
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.fillAmount = health / maxHealth;

        if(isReloading)
        {
            
            if (currentReloadTime > 0)
            {
                reloadTopText.gameObject.SetActive(true);
                reloadHead.gameObject.SetActive(true);
                reloadHead.fillAmount = currentReloadTime / reloadTime;
                currentReloadTime -= Time.deltaTime;
            } else
            {
                reloadTopText.gameObject.SetActive(false);
                reloadTop.gameObject.SetActive(true);
                reloadHead.gameObject.SetActive(false);
                isReloading = false;
                hasBullet = true;
            }
        }
    }

    private void FixedUpdate()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(gravity * Vector2.down);
    }

    void Jump(EventBody eb)
    {
        //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(gameObject.GetComponent<Rigidbody2D>().velocity.x, 0f);
        gameObject.GetComponent<Rigidbody2D>().AddForce(jumpForce * Vector2.up);
    }

    void Fire(EventBody eb)
    {
        Quaternion dir = Quaternion.identity;
        if(facingRight)
        {
            dir = Quaternion.Euler(0f, 0f, -90f);
        }
        else
        {
            dir = Quaternion.Euler(0f, 0f, 90f);
        }

        if(hasBullet)
        {
            reloadTop.gameObject.SetActive(false);
            GameObject she = Instantiate(shellyPrefab, gameObject.transform.position, dir);
            hasBullet = false;
        }
    }

    void MoveH(EventBody eb)
    {
        float dirX = ((AxisEB)eb).value;
        if(dirX > 0)
        {
            facingRight = true;
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
        if(dirX < 0)
        {
            facingRight = false;
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }

        if(dirX > 0f && gameObject.GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(dirX * speed * Vector2.right);
        }

        if (dirX < 0f && gameObject.GetComponent<Rigidbody2D>().velocity.x > -maxSpeed)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(dirX * speed * Vector2.right);
        }

    }

    void MoveV(EventBody eb)
    {
        float dirY = ((AxisEB)eb).value;
    }

    void Damage(EventBody eb)
    {
        health -= 1f;

        if(health < 1f)
        {
            SceneManager.LoadScene(2);
        }
    }

    void PickUpShelly(EventBody eb)
    {
        currentReloadTime = reloadTime;
        isReloading = true;
    }

}
