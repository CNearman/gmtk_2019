﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public GameObject bloodSplat;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameObject bloodPuff = Instantiate(bloodSplat, gameObject.transform.position, Quaternion.identity);
            Destroy(bloodPuff, 5f);
            EventManager.TriggerEvent(collision.gameObject.name + "_Damage", null);
            Destroy(gameObject);
        }
    }
}
