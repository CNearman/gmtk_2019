﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class BasicEnemyController : MonoBehaviour
    {
        public Rigidbody2D rb;
        public SpriteRenderer sr;
        public Transform target;

        public float MovementSpeed;

        public float AttackCooldown;
        public float VisionRange;
        public Transform Projectile;

        public float PatrolLeftFromStart;
        public float PatrolRightFromStart;

        public float health;

        [SerializeField]
        private bool IsFacingRight;

        private Vector3 StartingPosition;
        private Vector3 LeftBoundary;
        private Vector3 RightBoundary;

        private int currentState;

        private const int IDLE_STATE = 0;
        private const int WALKING_LEFT = 1;
        private const int WALKING_RIGHT = 2;
        private const int ATTACKING_LEFT = 3;
        private const int ATTACKING_RIGHT = 4;
        private const int ATTACKING_LEFT_COOLDOWN = 5;
        private const int ATTACKING_RIGHT_COOLDOWN = 6;

        private void Awake()
        {
            EventManager.StartListening(gameObject.name + "_Damage", Damage);
        }

        // Start is called before the first frame update
        private void Start()
        {
            target = GameObject.Find("Player").transform;
            currentState = WALKING_LEFT;
            StartingPosition = transform.position;
            LeftBoundary = StartingPosition + Vector3.left * PatrolLeftFromStart;
            RightBoundary = StartingPosition - Vector3.left * PatrolRightFromStart;
            sr = GetComponent<SpriteRenderer>();
        }

        // Update is called once per frame
        private void Update()
        {
            var currentPosition = transform.position;

            switch (currentState)
            {
                case WALKING_LEFT:
                    if (target.position.x <= currentPosition.x && (target.position - currentPosition).magnitude < VisionRange)
                    {
                        currentState = ATTACKING_LEFT;
                        break;
                    }
                    if (currentPosition.x <= LeftBoundary.x) // Passed Left Boundary
                    {
                        IsFacingRight = true;
                        currentState = WALKING_RIGHT;
                    }

                    Move();
                    break;
                case WALKING_RIGHT:
                    if (target.position.x >= currentPosition.x && (target.position - currentPosition).magnitude < VisionRange)
                    {
                        currentState = ATTACKING_RIGHT;
                        break;
                    }
                    if (currentPosition.x >= RightBoundary.x) // Passed Right Boundary
                    {
                        IsFacingRight = false;
                        currentState = WALKING_LEFT;
                    }

                    Move();
                    break;
                case ATTACKING_LEFT:
                    Instantiate(Projectile, currentPosition + Vector3.left * 0.5f, Quaternion.Euler(0,0, 180));
                    currentState = ATTACKING_LEFT_COOLDOWN;
                    break;
                case ATTACKING_RIGHT:
                    Instantiate(Projectile, currentPosition + Vector3.right * 0.5f, Quaternion.identity);
                    currentState = ATTACKING_RIGHT_COOLDOWN;
                    break;
                case ATTACKING_LEFT_COOLDOWN:
                    StartCoroutine("AttackCoolDown", WALKING_LEFT);
                    break;
                case ATTACKING_RIGHT_COOLDOWN:
                    StartCoroutine("AttackCoolDown", WALKING_RIGHT);
                    break;
            }
        }

        private void Move()
        {
            float xComponent = IsFacingRight ? MovementSpeed : -1 * MovementSpeed;
            if (IsFacingRight)
            {
                sr.flipX = true;
            }
            else
            {
                sr.flipX = false;
            }
            var currentVelocity = rb.velocity;
            currentVelocity.x = xComponent;
            rb.velocity = currentVelocity;
        }

        private IEnumerator AttackCoolDown(int targetState)
        {
            yield return WaitAndChangeState(targetState, AttackCooldown);
        }

        private IEnumerator WaitAndChangeState(int targetState, float waitDuration)
        {
            yield return new WaitForSeconds(AttackCooldown);
            currentState = targetState;
        }


        private void OnDrawGizmos()
        {
            Gizmos.DrawIcon(LeftBoundary, "Enemy1_64x64.png");
        }

        void Damage(EventBody eb)
        {
            health -= 1f;

            if (health < 1f)
            {
                Destroy(gameObject);
            }
        }
    }

}
