﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TntPlunger : MonoBehaviour
{
    public int tntWireGroup;

    private void Awake()
    {
        EventManager.StartListening("TriggerTnt_" + tntWireGroup, TriggerTnt);
    }

    void TriggerTnt(EventBody eb)
    {
        EventManager.TriggerEvent("BOOM_" + tntWireGroup, null);
    }
}
