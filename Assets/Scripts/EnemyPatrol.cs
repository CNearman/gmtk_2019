﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public Rigidbody2D rigidbody;

    public float MovementSpeed;

    public float PatrolLeftFromStart;
    public float PatrolRightFromStart;

    [SerializeField]
    private bool IsFacingRight;

    private Vector3 StartingPosition;
    private Vector3 LeftBoundary;
    private Vector3 RightBoundary;

    // Start is called before the first frame update
    private void Start()
    {
        StartingPosition = transform.position;
        LeftBoundary = StartingPosition + Vector3.left * PatrolLeftFromStart;
        RightBoundary = StartingPosition - Vector3.left * PatrolRightFromStart;
    }

    // Update is called once per frame
    private void Update()
    {
        var currentPosition = transform.position;
        if (currentPosition.x <= LeftBoundary.x) // Passed Left Boundary
        {
            IsFacingRight = true;
        }

        if (currentPosition.x >= RightBoundary.x) // Passed Right Boundary
        {
            IsFacingRight = false;
        }

        float xComponent = IsFacingRight ? MovementSpeed : -1 * MovementSpeed;

        var currentVelocity = rigidbody.velocity;
        currentVelocity.x = xComponent;
        rigidbody.velocity = currentVelocity;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(LeftBoundary, "Enemy1_64x64.png");
    }
}
