﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject target;
    public float minHorizontal;
    public float deadZone;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(target.transform.position.x > transform.position.x)
        {
            transform.position = new Vector3(target.transform.position.x, 1, -10);
        } else if (target.transform.position.x < transform.position.x)
        {
            transform.position = new Vector3(target.transform.position.x, 1, -10);
        }
    }
}
