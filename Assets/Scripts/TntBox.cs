﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TntBox : MonoBehaviour
{
    public int tntWireGroup;

    public GameObject explosion;

    private void Awake()
    {
        EventManager.StartListening("BOOM_" + tntWireGroup, BlowTnt);
    }

    void BlowTnt(EventBody eb)
    {
        GameObject ex = Instantiate(explosion, gameObject.transform.position, Quaternion.identity);
        Destroy(ex, 5f);
        Destroy(gameObject);
    }
}
