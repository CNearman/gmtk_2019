﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    [Range(1, 4)]
    private int playerNumber;

    // Update is called once per frame
    void Update()
    {
        //Button Events
        if (Input.GetButtonDown("A_" + playerNumber))
        {
            EventManager.TriggerEvent("A_" + playerNumber + "Pressed", null);
        }
        if (Input.GetButtonUp("A_" + playerNumber))
        {
            EventManager.TriggerEvent("A_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("X_" + playerNumber))
        {
            EventManager.TriggerEvent("X_" + playerNumber + "Pressed", null);
        }
        if (Input.GetButtonUp("X_" + playerNumber))
        {
            EventManager.TriggerEvent("X_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("B_" + playerNumber))
        {
            EventManager.TriggerEvent("B_" + playerNumber + "Pressed", null);
        }
        if (Input.GetButtonUp("B_" + playerNumber))
        {
            EventManager.TriggerEvent("B_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("Y_" + playerNumber))
        {
            EventManager.TriggerEvent("Y_" + playerNumber + "Pressed", null);
        }
        if (Input.GetButtonUp("Y_" + playerNumber))
        {
            EventManager.TriggerEvent("Y_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("RB_" + playerNumber))
        {
            EventManager.TriggerEvent("RB_" + playerNumber, null);
        }
        if (Input.GetButtonUp("RB_" + playerNumber))
        {
            EventManager.TriggerEvent("RB_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("LB_" + playerNumber))
        {
            EventManager.TriggerEvent("LB_" + playerNumber, null);
        }
        if (Input.GetButtonUp("LB_" + playerNumber))
        {
            EventManager.TriggerEvent("LB_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("Back_" + playerNumber))
        {
            EventManager.TriggerEvent("Back_" + playerNumber + "Pressed", null);
        }
        if (Input.GetButtonUp("Back_" + playerNumber))
        {
            EventManager.TriggerEvent("Back_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("Start_" + playerNumber))
        {
            EventManager.TriggerEvent("Start_" + playerNumber + "Pressed", null);
        }
        if (Input.GetButtonUp("Start_" + playerNumber))
        {
            EventManager.TriggerEvent("Start_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("LS_" + playerNumber))
        {
            EventManager.TriggerEvent("LS_" + playerNumber + "Pressed", null);
        }
        if (Input.GetButtonUp("LS_" + playerNumber))
        {
            EventManager.TriggerEvent("LS_" + playerNumber + "Released", null);
        }

        if (Input.GetButtonDown("RS_" + playerNumber))
        {
            EventManager.TriggerEvent("RS_" + playerNumber + "Pressed", null);
        }
        if (Input.GetButtonUp("RS_" + playerNumber))
        {
            EventManager.TriggerEvent("RS_" + playerNumber + "Released", null);
        }


        //L stick
        float rawLXAxis = Input.GetAxisRaw("L_XAxis_" + playerNumber);
        float rawLYAxis = Input.GetAxisRaw("L_YAxis_" + playerNumber);

        EventManager.TriggerEvent("L_XAxis_" + playerNumber, new AxisEB { value = rawLXAxis });
        EventManager.TriggerEvent("L_YAxis_" + playerNumber, new AxisEB { value = rawLYAxis });

        //R Stick
        float rawRXAxis = Input.GetAxisRaw("R_XAxis_" + playerNumber);
        float rawRYAxis = Input.GetAxisRaw("R_YAxis_" + playerNumber);

        EventManager.TriggerEvent("R_XAxis_" + playerNumber, new AxisEB { value = rawRXAxis });
        EventManager.TriggerEvent("R_YAxis_" + playerNumber, new AxisEB { value = rawRYAxis });

        //D pad
        float rawDXAxis = Input.GetAxisRaw("DPad_XAxis_" + playerNumber);
        float rawDYAxis = Input.GetAxisRaw("DPad_YAxis_" + playerNumber);

        EventManager.TriggerEvent("DPad_XAxis_" + playerNumber, new AxisEB { value = rawDXAxis });
        EventManager.TriggerEvent("DPad_YAxis_" + playerNumber, new AxisEB { value = rawDYAxis });

        //Triggers
        float rawTRAxis = Input.GetAxisRaw("TriggersR_" + playerNumber);
        float rawTLAxis = Input.GetAxisRaw("TriggersL_" + playerNumber);

        EventManager.TriggerEvent("TriggersR_" + playerNumber, new AxisEB { value = rawTRAxis });
        EventManager.TriggerEvent("TriggersL_" + playerNumber, new AxisEB { value = rawTLAxis });
    }

    void FixedUpdate()
    {
        //This one is specifically for movement based on rigidbody forces
        EventManager.TriggerEvent("Move_" + playerNumber, new AxisEB { value = Input.GetAxisRaw("L_XAxis_" + playerNumber) });
    }
}