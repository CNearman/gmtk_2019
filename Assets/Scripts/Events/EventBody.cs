﻿// These represent the various event information. Event body is the base class, then depending on what information needs to be passed in the event, a different event body is used. Example ones are below.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBody
{

}

// Used for controller input. The value of the axis is passed, how far the control stick is tilted in a direction.
public class AxisEB : EventBody
{
    public float value;
}