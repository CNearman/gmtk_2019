﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolicProjectileMotion : MonoBehaviour
{
    public float MoveSpeed;
    public float MaxVerticalVelocity;
    public float VelocityDelta;
    private float CurrentVelocity;
    // Start is called before the first frame update
    void Start()
    {
        CurrentVelocity = MaxVerticalVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        var upvector = Vector3.up * CurrentVelocity;
        CurrentVelocity = CurrentVelocity - VelocityDelta;
        var forwardVector = Vector3.right;
        transform.Translate((forwardVector + upvector) * MoveSpeed, Space.Self);
    }
}
